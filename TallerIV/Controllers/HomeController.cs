﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TallerIV.Models;

namespace TallerIV.Controllers
{
    public class HomeController : Controller
    {
        private TallerIVContext db = new TallerIVContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Mensaje enviado por ViewBag.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Modificando mensaje en ventana de contacto";

            return View();
        }
        public ActionResult TestView()
        {
            return View();
        }
        public ActionResult VistaTipada()
        {
            List<User> users = db.Users.ToList();

            return View(users);
        }
        public ActionResult tipada()
        {
            List<User> users2 = (from u in db.Users
                         select u).ToList();

            List<User> users = db.Users.ToList();

            return View(users);
        }
        [HttpGet]
        public ActionResult Creacion()
        {
            List<User> users = db.Users.ToList();

            SelectList s = new SelectList(users);

            ViewBag.Usuarios = s;


            return View();
        }
        [HttpPost]
        public ActionResult Creacion(User user)
        {
            if(ModelState.IsValid)
            {
                db.Users.Add(user);
                db.SaveChanges();
                return RedirectToAction("tipada");
            }

            return View(user);
        }
        [HttpGet]
        public ActionResult CrearUsuarioPais()
        {
            UsuarioPaisViewModel up = new UsuarioPaisViewModel();
            return View(up);
        }
        [HttpPost]
        public ActionResult CrearUsuarioPais(UsuarioPaisViewModel up)
        {

            if(ModelState.IsValid){
                User u = new User();
                u.Username = up.Username;
                u.Password = up.Password;

                Pais p = new Pais();
                p.Nombre = up.Pais;

                db.Users.Add(u);

                db.Paises.Add(p);
                db.SaveChanges();

                return RedirectToAction("VistaTipada");
            }

            return View(up);
        }
        [HttpPost]
        public JsonResult ajaxExample(string username)
        {
            User user = db.Users.Where(model => model.Username == username).FirstOrDefault();
            return Json(user);
        }
        public string ajaxString() {
            return "Hola a todos";
        }
        public string ajaxWithParams(string usuario)
        {
            User us = db.Users.Where(u => u.Username == usuario).FirstOrDefault();
            return us.Password;
        }
        [HttpPost]
        public JsonResult ajaxJson(string usuario)
        {
            User us = db.Users.Where(u => u.Username == usuario).FirstOrDefault();
            return Json(us);
        }

        public ActionResult ajaxPartialView(string usuario)
        {
            User us = db.Users.Where(u => u.Username == usuario).FirstOrDefault();
            return PartialView("_ajaxPartialView",us);
        }
    }
}