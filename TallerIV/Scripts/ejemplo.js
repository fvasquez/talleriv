﻿$(document).ready(function () {
    $("#otroBoton").click(function () {
        $.ajax({
            // the URL for the request
            url: "/home/ajaxPartialView",

            // the data to send (will be converted to a query string)
            data: {
                usuario: $("#buscadorUsuarios").val(),
            },

            // whether this is a POST or GET request
            type: "GET",

            // the type of data we expect back
            dataType: "html",

            // code to run if the request succeeds;
            // the response is passed to the function
            success: function (response) {
                //$(".jumbotron").append("<p>La contraseña es: "+ response.Username +"</p>"); // Para cuando viene como Json
                $("#contenedorDetalle").html(response);
            },

            // code to run if the request fails; the raw request and
            // status codes are passed to the function
            error: function (xhr, status, errorThrown) {
                alert("Hay un error: " + errorThrown);
            },

            // code to run regardless of success or failure
            complete: function (xhr, status) {
                console.log(status);
            }
        });
    });
   
});