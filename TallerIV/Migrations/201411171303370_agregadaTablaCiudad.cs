namespace TallerIV.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class agregadaTablaCiudad : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Ciudads",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                        Pais_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Pais", t => t.Pais_Id)
                .Index(t => t.Pais_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Ciudads", "Pais_Id", "dbo.Pais");
            DropIndex("dbo.Ciudads", new[] { "Pais_Id" });
            DropTable("dbo.Ciudads");
        }
    }
}
