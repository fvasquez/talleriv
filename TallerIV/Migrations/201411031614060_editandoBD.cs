namespace TallerIV.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editandoBD : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Pais",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Pais");
        }
    }
}
