namespace TallerIV.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class agregadaTablaCiudadWithFK : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Ciudads", "Pais_Id", "dbo.Pais");
            DropIndex("dbo.Ciudads", new[] { "Pais_Id" });
            RenameColumn(table: "dbo.Ciudads", name: "Pais_Id", newName: "PaisId");
            AlterColumn("dbo.Ciudads", "PaisId", c => c.Int(nullable: false));
            CreateIndex("dbo.Ciudads", "PaisId");
            AddForeignKey("dbo.Ciudads", "PaisId", "dbo.Pais", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Ciudads", "PaisId", "dbo.Pais");
            DropIndex("dbo.Ciudads", new[] { "PaisId" });
            AlterColumn("dbo.Ciudads", "PaisId", c => c.Int());
            RenameColumn(table: "dbo.Ciudads", name: "PaisId", newName: "Pais_Id");
            CreateIndex("dbo.Ciudads", "Pais_Id");
            AddForeignKey("dbo.Ciudads", "Pais_Id", "dbo.Pais", "Id");
        }
    }
}
