﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TallerIV.Models
{
    public class UsuarioPaisViewModel
    {
        [Required]
        [Display(Name = "Usuario")]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Pais { get; set; }
    }
}