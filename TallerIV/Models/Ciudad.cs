﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace TallerIV.Models
{
    public class Ciudad
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }

        public int PaisId { get; set; }

        [ForeignKey("PaisId")]
        public virtual Pais Pais { get; set; }
    }
}
