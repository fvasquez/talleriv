﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TallerIV.Models
{
    public class TallerIVContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Pais> Paises { get; set; }
        public System.Data.Entity.DbSet<TallerIV.Models.Ciudad> Ciudades { get; set; }
    }
}