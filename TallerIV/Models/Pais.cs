﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TallerIV.Models
{
    public class Pais
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }

        public List<Ciudad> Ciudades { get; set; }
    }
}